module.exports = {
  extends: ['standard', 'standard-react'],
  globals: {
    fetch: true,
    alert: true
  }
};