import React from 'react'
import moment from 'moment'

import FilterTable from './FilterTable'
import OrderTable from './OrderTable'
import OrderForm from './OrderForm'

export default class Homepage extends React.Component {
  constructor (props) {
    super(props)

    this.updateSearch = this.updateSearch.bind(this)
    this.updateFilter = this.updateFilter.bind(this)
    this.addNewOrder = this.addNewOrder.bind(this)
    this.deleteOrder = this.deleteOrder.bind(this)
    this.updateOrder = this.updateOrder.bind(this)

    this.state = {
      orders: [],
      allOrders: [],
      users: [],
      products: [],
      ordersFrom: 2,
      search: ''
    }
  }

  componentDidMount () {
    fetch('json/orders.json') // eslint-disable-line no-undef
      .then(response => response.json())
      .then(orders => this.setState({ orders, allOrders: orders }))
    fetch('json/users.json') // eslint-disable-line no-undef
      .then(response => response.json())
      .then(users => this.setState({ users }))
    fetch('json/products.json') // eslint-disable-line no-undef
      .then(response => response.json())
      .then(products => this.setState({ products }))
  }

  updateSearch () {
    let search = this.refs.search.value
    let orders

    if (search !== this.state.search) {
      this.setState({ search })
      orders = this.search(search, this.state.allOrders)
      orders = this.filter(this.state.ordersFrom, orders)
      this.setState({ orders })
    }
  }

  search (search, orders) {
    search = search.toLowerCase()
    return orders.filter(order =>
      order.user.toLowerCase().indexOf(search) > -1 ||
      order.name.toLowerCase().indexOf(search) > -1)
  }

  updateFilter (ordersFrom) {
    const { allOrders, search } = this.state
    let orders

    if (ordersFrom !== this.state.ordersFrom) {
      this.setState({ ordersFrom })
      orders = this.filter(ordersFrom, allOrders)
      orders = this.search(search, orders)
      this.setState({ orders })
    }
  }

  filter (ordersFrom, orders) {
    if (ordersFrom === 0) {
      let today = moment().format('YYYY-MM-DD')
      return orders.filter(order => order.date === today)
    } else if (ordersFrom === 1) {
      let week = moment().subtract(7, 'days').format('YYYY-MM-DD')
      return orders.filter(order => order.date >= week)
    } else {
      return orders
    }
  }

  addNewOrder (order) {
    const { allOrders, search, ordersFrom } = this.state
    let newOrders = [ ...allOrders, order ]
    newOrders = this.search(search, newOrders)
    newOrders = this.filter(ordersFrom, newOrders)
    this.setState({
      orders: newOrders,
      allOrders: [ ...allOrders, order ]
    })
  }

  deleteOrder (orderId) {
    let { allOrders, orders } = this.state

    const ask = confirm('Are you sure you want to delete this order?') // eslint-disable-line no-undef
    if (ask) {
      allOrders = allOrders.filter(order => order.id !== orderId)
      orders = orders.filter(order => order.id !== orderId)

      this.setState({ orders, allOrders })
    }
  }

  updateOrder (updated) {
    const { allOrders, orders } = this.state
    this.setState({
      orders: orders.map(order => (order.id !== updated.id) ? order : updated),
      allOrders: allOrders.map(order => (order.id !== updated.id) ? order : updated)
    })
  }

  render () {
    const { orders, ordersFrom, users, products, allOrders } = this.state

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm'>
            <OrderForm users={users} products={products} orders={allOrders} addNewOrder={this.addNewOrder} />
            <FilterTable selected={ordersFrom} updateFilter={this.updateFilter} />
            <input
              type='search'
              className='form-control search'
              placeholder='Search for products or users...'
              ref='search'
              onKeyUp={this.updateSearch} />
            <OrderTable
              orders={orders}
              users={users}
              products={products}
              updateOrder={this.updateOrder}
              deleteOrder={this.deleteOrder} />
          </div>
        </div>
      </div>
    )
  }
}
