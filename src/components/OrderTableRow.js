import React from 'react'

export default class OrderTableRow extends React.Component {
  constructor (props) {
    super(props)

    this.changeEdit = this.changeEdit.bind(this)
    this.onSave = this.onSave.bind(this)

    this.state = { editing: false }
  }

  onDelete (e, id, deleteOrder) {
    e.preventDefault()
    deleteOrder(id)
  }

  changeEdit (e) {
    if (e) {
      e.preventDefault()
    }

    const editing = !this.state.editing
    const { order } = this.props
    this.setState({ editing })

    if (editing && this.refs.date && this.refs.price) {
      this.refs.date.value = order.date
      this.refs.price.value = order.price
    }
  }

  onSave (e) {
    e.preventDefault()

    const { order, users, products, updateOrder } = this.props
    const date = this.refs.date.value
    const price = this.refs.price.value
    let user = parseInt(this.refs.user.value)
    let product = parseInt(this.refs.product.value)

    if (user && product && date && price) {
      user = users.find(obj => obj.id === user)
      product = products.find(obj => obj.id === product)
      updateOrder({
        id: order.id,
        user: user.name,
        name: product.name,
        date,
        price
      })
      this.changeEdit()
    } else {
      alert('All fields are required') // eslint-disable-line no-undef
    }
  }

  render () {
    const { order, users, products, deleteOrder } = this.props
    if (!this.state.editing) {
      return (
        <tr>
          <td>{order.user}</td>
          <td>{order.name}</td>
          <td>{order.date}</td>
          <td>{order.price}</td>
          <td>
            <a href='#' onClick={e => this.onDelete(e, order.id, deleteOrder)}>Remove</a>&nbsp;
            <a href='#' onClick={e => this.changeEdit(e)}>Edit</a>
          </td>
        </tr>
      )
    } else {
      return (
        <tr>
          <td>
            <select className='form-control' ref='user'>
              {users.map(user => (
                <option key={user.id} value={user.id} selected={user.name === order.user}>{user.name}</option>
              ))}
            </select>
          </td>
          <td>
            <select className='form-control' ref='product'>
              {products.map(product => (
                <option key={product.id} value={product.id} selected={product.name === order.name}>{product.name}</option>
              ))}
            </select>
          </td>
          <td><input className='form-control' type='date' defaultValue={order.date} ref='date' /></td>
          <td><input className='form-control' type='number' defaultValue={order.price} ref='price' /></td>
          <td>
            <a href='#' onClick={e => this.changeEdit(e)}>Cancel</a>&nbsp;
            <a href='#' onClick={e => this.onSave(e)}>Save</a>
          </td>
        </tr>
      )
    }
  }
}

OrderTableRow.propTypes = {
  order: React.PropTypes.object,
  users: React.PropTypes.array,
  products: React.PropTypes.array,
  updateOrder: React.PropTypes.func,
  deleteOrder: React.PropTypes.func
}
