import React from 'react'
import moment from 'moment'

export default class OrderForm extends React.Component {
  constructor (props) {
    super(props)

    this.onButtonSubmit = this.onButtonSubmit.bind(this)
  }

  onButtonSubmit (e) {
    e.preventDefault()

    const { users, products, orders, addNewOrder } = this.props
    const quantity = this.refs.quantity.value
    let user = parseInt(this.refs.user.value)
    let product = parseInt(this.refs.product.value)

    if (user && product && quantity) {
      this.refs.quantity.value = ''
      user = users.find(obj => obj.id === user)
      product = products.find(obj => obj.id === product)
      addNewOrder({
        id: orders.reduce((prev, curr) => (prev.id > curr.id) ? prev.id : curr.id) + 1,
        date: moment().format('YYYY-MM-DD'),
        name: product.name,
        user: user.name,
        price: quantity * product.price
      })
    } else {
      alert('All fields are required') // eslint-disable-line no-undef
    }
  }

  render () {
    const { users, products } = this.props

    return (
      <div className='jumbotron jumbotron-fluid row justify-content-sm-center'>
        <div className='col-sm-6'>
          <h3>Add new order</h3>
          <div className='form-group row'>
            <label htmlFor='user' className='col-sm-2 col-form-label'>User</label>
            <div className='col-sm-10'>
              <select className='form-control' id='user' ref='user'>
                {users.map(user => <option key={user.id} value={user.id}>{user.name}</option>)}
              </select>
            </div>
          </div>
          <div className='form-group row'>
            <label htmlFor='product' className='col-sm-2 col-form-label'>Product</label>
            <div className='col-sm-10'>
              <select className='form-control' id='product' ref='product'>
                {products.map(product => <option key={product.id} value={product.id}>{product.name}</option>)}
              </select>
            </div>
          </div>
          <div className='form-group row'>
            <label htmlFor='quantity' className='col-sm-2 col-form-label'>Quantity</label>
            <div className='col-sm-10'>
              <input type='number' className='form-control' id='quantity' ref='quantity' />
            </div>
          </div>
          <button type='submit' className='btn btn-primary' onClick={e => this.onButtonSubmit(e)}>Save</button>
        </div>
      </div>
    )
  }
}

OrderForm.propTypes = {
  users: React.PropTypes.array,
  products: React.PropTypes.array,
  orders: React.PropTypes.array,
  addNewOrder: React.PropTypes.func
}
