import React from 'react'

const FilterTable = ({ selected, updateFilter }) => {
  return (
    <div className='navbar navbar-expand-lg navbar-light bg-light filter-table'>
      <span className='navbar-brand'>Get orders from</span>
      <div className='collapse navbar-collapse'>
        <div className='form-check nav-item'>
          <label className='form-check-label'>
            <input
              className='form-check-input'
              type='radio'
              name='time'
              id='today'
              checked={selected === 0}
              onChange={() => updateFilter(0)} />
            Today
          </label>
        </div>
        <div className='form-check nav-item'>
          <label className='form-check-label'>
            <input
              className='form-check-input'
              type='radio'
              name='time'
              id='week'
              checked={selected === 1}
              onChange={() => updateFilter(1)} />
            This week
          </label>
        </div>
        <div className='form-check nav-item'>
          <label className='form-check-label'>
            <input
              className='form-check-input'
              type='radio'
              name='time'
              id='all'
              checked={selected === 2}
              onChange={() => updateFilter(2)} />
            All time
          </label>
        </div>
      </div>
    </div>
  )
}

FilterTable.propTypes = {
  selected: React.PropTypes.number.isRequired,
  updateFilter: React.PropTypes.func
}

export default FilterTable
