import React from 'react'

import OrderTableRow from './OrderTableRow'

const renderRows = (orders, users, products, updateOrder, deleteOrder) => {
  return orders.map(order => (
    <OrderTableRow
      key={order.id}
      order={order}
      users={users}
      products={products}
      updateOrder={updateOrder}
      deleteOrder={deleteOrder} />
  ))
}

const OrderTable = ({ orders, users, products, updateOrder, deleteOrder }) => {
  return (
    <table className='table table-striped'>
      <thead>
        <th>User</th>
        <th>Product</th>
        <th>Date</th>
        <th>Price</th>
        <th>Actions</th>
      </thead>
      <tbody>
        {renderRows(orders, users, products, updateOrder, deleteOrder)}
      </tbody>
    </table>
  )
}

OrderTable.propTypes = {
  orders: React.PropTypes.array,
  users: React.PropTypes.array,
  products: React.PropTypes.array,
  updateOrder: React.PropTypes.func,
  deleteOrder: React.PropTypes.func
}

export default OrderTable
